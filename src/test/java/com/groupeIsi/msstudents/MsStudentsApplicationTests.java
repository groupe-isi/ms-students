package com.groupeIsi.msstudents;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.assertThat;

@SpringBootTest
class MsStudentsApplicationTests {

	@Test
	public void assertThatIsEasyToReadFromLeftToRight() {
        int expectedValue = 2;

        assertThat(compute(), is(expectedValue));
    }

}
